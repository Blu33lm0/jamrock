<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- JQuery datepicker styles -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <!-- JQuery -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/jquery.autofill.min.js"></script>

    <title>Home - Registration</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />
    <!-- Bootstrap local -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">

    <!-- Bootstrap Form Helpers local -->
    <link href="css/bootstrap-form-helpers.min.css" rel="stylesheet" media="screen">

    <!-- Custom styles for this template -->
    <style type="text/css">
        body {
            padding-top: 4rem;
        }

        textarea {
            resize: none;
        }

        .starter-template {
            padding: 1rem 0.5rem;
            text-align: center;
        }

    </style>
</head>
