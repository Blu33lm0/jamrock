<?php 
    require_once '../include/connect.php';
    require_once '../include/head.php';
?>
 
<?php 
    //This gets the user_id value that was passe3d via query string, to the view.php page
    $userid = $_GET['user_id'];
    //This constructs a query to get all users (should be one) with the user_id value
    $query = "SELECT * from users_tb where `user_id`= '$userid' ";
    //THis constructs the query and executes it. 
    $command = mysqli_query($connect, $query);
    //If the command fails, then print an error message and kill the execution of the code
    if(!$command){
        echo 'Select Query Failed: '.mysqli_error($connect);
        die();
    }
 
    //store the result of the command to a variable. THis will store an array
    $result = mysqli_fetch_row($command);
     
    //Check if data was returned with the command. If not, then show an error stating that the record was not found 
    if(!$result){    
        echo '<div class="alert alert-danger">Record Not Found! </div>';
        die();
    }
 
 
    ?>
    <!-- Now that $result has been populated with the data, you may access the values using the array subscripts. It is best practice to store the values in variables, and use the variables for computation.  -->
    <div class="alert alert-success">Now Viewing:  <?php echo $result[1] . ' ' . $result[2]. ' ' ?> Profile</div>
        <table class="table table-sm">
            <tr>
        <td>
            <?php echo '<div class="text-secondary">Gender: </div>'.$result[3]; ?>
        </td>
    </tr>
    <tr>
        <td>
            <?php echo '<div class="text-secondary">Born on: </div>'.$result[4]; ?>
        </td>
    </tr>
    <tr>
        <td>
            <?php echo '<div class="text-secondary">Country: </div>'.$result[5]; ?>
        </td>
    </tr>
    <tr>
        <td>
            <?php echo '<div class="text-secondary">Image: </div>'.$result[6]; ?>
        </td>
    </tr>
    <tr>
        <td>
            <?php echo '<div class="text-secondary">Address: </div>'.$result[7]; ?>
        </td>
    </tr>
    <tr>
        <td>
            <?php echo '<div class="text-secondary">Phone Number: </div>'.$result[8]; ?>
        </td>
    </tr>
    <tr>
        <td>
            <?php echo '<div class="text-secondary">Email: </div>'.$result[9]; ?>
        </td>
    </tr>
    <tr>
        <td>
            <?php echo '<div class="text-secondary">Username: </div>'.$result[10]; ?>
        </td>
    </tr>
        </table>
        <br/>
        <a class="text-left" href="../setting_admin.php">Return to List</a>
