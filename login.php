<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login - JamRock</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/login.css">

    <body>
        <div class="loginbox">
            <img src="img/avatar.png" class="avatar">
            <h1>Login</h1>
            <form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post" enctype="multipart/form-data">
               <?php include 'session/login_session.php'; ?>
                <p>Username</p>
                <input type="text" name="username" placeholder="Enter Username">
                <?php
                    if(isset($errors['username'])){
                        echo "<em class = 'text-danger'>".$errors['username']."</em>";
                    }
                ?>
                <p>Password</p>
                <input type="password" name="password" placeholder="Enter Password">
                <?php
                    if(isset($errors['password'])){
                        echo "<em class = 'text-danger'>".$errors['password']."</em>";
                    }
                ?>
                <input type="submit" name="login_user" value="login">
                <span>Don't have an account?</span>
                <a href="signup.php">Sign up!</a><br/>
                <a class="btn-arrow-left" href="index.php">Back</a>
            </form>

        </div>

    </body>
</head>

</html>