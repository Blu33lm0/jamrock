<?php 
    include_once ('include/connect.php');

    $query = "SELECT * from users_tb";
    $command = mysqli_query($connect, $query);
    if(!$command){
        echo 'Select Query Failed: '.mysqli_error($connect);
        die();
    }

    include 'include/head.php';
?>

<body>
    <div class="starter-template">
        <h1 class="text-primary">List Of Registered Users</h1>
    </div>
    <!-- Create table for display to users. -->
    <table class="table table-condensed table-bordered table-dark table-responsive">
        <tr class="text-warning">
            <th>Name</th>
            <th>Gender</th>
            <th>Date Of Birth</th>
            <th>Country</th>
            <th>Profile Picture</th>
            <th>Address</th>
            <th>Phone</th>
            <th>Email</th>
            <th>User Name</th>
        </tr>

        <?php
    //gets each row of data from the database 
  while($row = mysqli_fetch_array($command)){
        echo '<tr>';
        echo '<td>'.$row['first_name']." ".$row['last_name'].'</td>';
        echo '<td>'.$row['gender'].'</td>';
        echo '<td>'.$row['DOB'].'</td>';
        echo '<td>'.$row['country'].'</td>';
        echo '<td>'.$row['picture'].'</td>';
        echo '<td>'.$row['address'].'</td>';
        echo '<td>'.$row['phone_number'].'</td>';
        echo '<td>'.$row['email'].'</td>';
        echo '<td>'.$row['user_name'].'</td>';
        echo "<td><a href='crud/view.php?user_id=".$row['user_id']."'>View</a></td>";
        echo "<td><a href='crud/edit.php?user_id=".$row['user_id']."'>Edit</a></td>";
        echo "<td><a href='crud/delete.php?user_id=".$row['user_id']."'>Delete</a></td>";
        echo "<td><a href='crud/create.php?user_id=".$row['user_id']."'>Create</a></td>";
        echo '</tr>';
    }
?>
    </table>
<a class="text-left" href="../blog/profile.php">Return home</a>
</body>

<?php include 'include/footer.php' ?>
