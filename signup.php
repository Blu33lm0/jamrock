<?php 
    include_once 'assets/errors.php';
    include 'include/head.php';
?>

<!-- /.container -->

<body>
    <main role="main" class="container">

        <div class="starter-template">
            <h1 class="text-primary">Registration Form</h1>
            <p class="lead">Please fill all section of this form to register.</p>
            <?php
                    if(isset($success['reg'])){
                        echo "<div class= 'text-success'>".$success['reg']."</div>";
                    }
                ?>
        </div>
    </main>
    <div class="container">
        <form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="first_name">First Name:</label>
                <input type="text" name="first_name" id="first_name" class="form-control" required>
                <?php
                    if(isset($error['first_name'])){
                        echo "<div class= 'text-danger'>".$error['first_name']."</div>";
                    }
                ?>
            </div>

            <div class="form-group">
                <label for="last_name">Last Name:</label>
                <input type="text" name="last_name" id="last_name" class="form-control" required>
                <?php
                    if(isset($error['last_name'])){
                        echo "<div class= 'text-danger' >".$error['last_name']."</div>";
                    }
                ?>
            </div>

            <div class="form-group">
                <label class="control-label" for="date_of_birth">Date Of Birth:</label>
                <input type="date" name="date_of_birth" id="date_of_birth" class="form-control" placeholder="mm/dd/yyyy">
            </div>
            <div class="form-group">
                <label>Location:</label>
                <select id="countries_phone" class="form-control bfh-countries" data-country="JM" data-flag="true" data-role="country-selector" name="location"></select>
            </div>

            <div class="form-group">
                <label>Telephone #:</label>
                <input type="tel" class="form-control bfh-phone" data-country="countries_phone" name="telephone">
            </div>

            <div class="form-group">
                <label for="address">Address:</label>
                <input type="text" name="address" id="address" class="form-control">
            </div>

            <div class="form-group">
                <label for="gender">Gender:</label>
                <select name="gender" class="form-control">
                    <option value="select" selected>Select</option>
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                </select>
                <?php
                    if(isset($error['gender'])){
                        echo "<div class= 'text-danger'>".$error['gender']."</div>";
                    }
                ?>
            </div>

            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" placeholder="sample@example.com" name="email" id="email" class="form-control">
                <?php
                    if(isset($error['email'])){
                        echo "<div class= 'text-danger'>".$error['email']."</div>";
                    }
                ?>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-2" for="image">Upload Photo:</label>
                <div class="col-sm-10">
                    <span class="btn btn-default btn-file">
                    <input type="file" name="image">
                </span>
                </div>
            </div>

            <div class="form-group">
                <label for="user_name">User Name:</label>
                <input type="text" name="user_name" id="user_name" class="form-control" readonly>
            </div>

            <div class="form-group">
                <label for="password">Password:</label>
                <input type="password" name="password" id="password" class="form-control" required>
            </div>

            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-success btn-block text-uppercase">Submit</button>
            </div>

            <div class="form-group">
                <a href="index.php">Back</a>
            </div>
        </form>
    </div>
    <!-- /.container -->

    <script type="text/javascript">
        $().ready(function() {
            $("#email").autofill({
                fieldId: "user_name",
                overrideFieldEverytime: true
            });
        });

    </script>

    <?php include 'include/footer.php' ?>
