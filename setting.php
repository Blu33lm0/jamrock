<?php 
    include 'include/head.php';
    include 'include/connect.php';

    session_start();
    //constructs a query
    $user_name = $_SESSION['username'];
    $query = "SELECT * from users_tb WHERE user_name = '$user_name'"; 
    //executes query 
    $command = mysqli_query($connect, $query);
    //If the command fails, then print an error message and kill the execution of the code
    if(!$command){
        echo 'Select Query Failed: '.mysqli_error($connect);
        die();
    }
 
    //store the result of the command to a variable. THis will store an array
    $result = mysqli_fetch_row($command);
     
    //Check if data was returned with the command. If not, then show an error stating that the record was not found 
    if(!$result){    
        echo '<div class="alert alert-danger">Record Not Found! </div>';
        die();
    }
 
 
    ?>
<div class="starter-template">
    <h1 class="text-info">Profile</h1>
</div>
<!-- displaying info from database  -->
<div class="alert alert-success">Welcome,
    <?php echo ' '.$result[1] . ' ' . $result[2] ?>
</div>
<table class="table table-sm table-striped">
    <tr>
        <td>
            <?php echo '<div class="text-secondary">Gender: </div>'.$result[3]; ?>
        </td>
    </tr>
    <tr>
        <td>
            <?php echo '<div class="text-secondary">Born on: </div>'.$result[4]; ?>
        </td>
    </tr>
    <tr>
        <td>
            <?php echo '<div class="text-secondary">Country: </div>'.$result[5]; ?>
        </td>
    </tr>
    <tr>
        <td>
            <?php echo '<div class="text-secondary">Image: </div>'.$result[6]; ?>
        </td>
    </tr>
    <tr>
        <td>
            <?php echo '<div class="text-secondary">Address: </div>'.$result[7]; ?>
        </td>
    </tr>
    <tr>
        <td>
            <?php echo '<div class="text-secondary">Phone Number: </div>'.$result[8]; ?>
        </td>
    </tr>
    <tr>
        <td>
            <?php echo '<div class="text-secondary">Email: </div>'.$result[9]; ?>
        </td>
    </tr>
    <tr>
        <td>
            <?php echo '<div class="text-secondary">Username: </div>'.$result[10]; ?>
        </td>
    </tr>
</table>
<br/>
<a class="text-left" href="../blog/profile.php">Return home</a>

<?php include 'include/footer.php' ?>